# -*- coding: utf-8 -*-
import os, importlib, sys, pickle
import matplotlib.pylab as plt

import seaborn as sns
sns.set_style("whitegrid")
#
from matplotlib import rc
rc('text', usetex=True)

from matplotlib import rcParams
rcParams['font.family'] = "Times New Roman"

import regional_panels_on_map as regional_panels_on_map; importlib.reload(regional_panels_on_map)

pkl_file = open('srex_dict_py3.pkl', 'rb')
srex = pickle.load(pkl_file, encoding='latin1')	;	pkl_file.close()
polygons=srex.copy()
polygons['global']={'points':[(-180,-5),(180,-5),(180,5),(-180,5)]}

srex_reg_info={'ALA':{'color':'#006400','pos_off':(+0,+3)},
		'WNA':{'color':'darkblue','pos_off':(+20,+15)},
		'CNA':{'color':'#838B8B','pos_off':(+8,-4)},
		'ENA':{'color':'#006400','pos_off':(+23,-5)},
		'CGI':{'color':'darkcyan','pos_off':(+0,-5)},

		'CAM':{'color':'darkcyan','pos_off':(-5,-5)},
		'WSA':{'color':'#006400','pos_off':(0,0)},
		'AMZ':{'color':'darkblue','pos_off':(-2,0)},
		'NEB':{'color':'#838B8B','pos_off':(+8,-3)},
		'SSA':{'color':'darkcyan','pos_off':(+10,-4)},

		'SAH':{'color':'darkcyan','pos_off':(0,0)},
		'WAF':{'color':'#838B8B','pos_off':(0,-3)},
		'EAF':{'color':'#006400','pos_off':(0,-3)},
		'SAF':{'color':'darkblue','pos_off':(0,0)},

		'NEU':{'color':'#006400','pos_off':(-23,+5)},
		'CEU':{'color':'darkblue','pos_off':(+6,+7)},
		'CAS':{'color':'#006400','pos_off':(-6,+13)},
		'NAS':{'edge':'red','color':'none','pos_off':(-6,+11)},
		'TIB':{'color':'darkcyan','pos_off':(+5,+4)},
		'EAS':{'color':'#006400','pos_off':(+3,0)},
		'SAS':{'color':'darkcyan','pos_off':(0,0)},
		'MED':{'color':'#838B8B','pos_off':(-20,+5)},
		'WAS':{'color':'darkcyan','pos_off':(-7,-5)},
		'SAS':{'color':'darkblue','pos_off':(0,-3)},

		'SEA':{'color':'darkcyan','pos_off':(+5,+1)},
		'NAU':{'color':'darkblue','pos_off':(-25,3)},
		'SAU':{'color':'#006400','pos_off':(+4,+3)},
		'global':{'edge':'none','color':'none','alpha':1,'pos':(-140,+10),'scaling_factor':1.3}}

info_dict = {'ALA':{'values':range(10),'color':'red'},
		'WNA':{'values':[1,2,3,45],'color':'blue'},
		'CNA':{'values':range(10),'color':'red'},
		'ENA':{'values':range(10),'color':'red'},
		'CGI':{'values':range(10),'color':'red'},

		'CAM':{'values':range(10),'color':'red'},
		'WSA':{'values':range(10),'color':'red'},
		'AMZ':{'values':range(10),'color':'red'},
		'NEB':{'values':range(10),'color':'red'},
		'SSA':{'values':range(10),'color':'red'},

		'SAH':{'values':range(10),'color':'red'},
		'WAF':{'values':range(10),'color':'red'},
		'EAF':{'values':range(10),'color':'red'},
		'SAF':{'values':range(10),'color':'red'},

		'NEU':{'values':range(10),'color':'red'},
		'CEU':{'values':range(10),'color':'red'},
		'CAS':{'values':range(10),'color':'red'},
		'NAS':{'values':range(10),'color':'red'},
		'TIB':{'values':range(10),'color':'red'},
		'EAS':{'values':range(10),'color':'red'},
		'SAS':{'values':range(10),'color':'red'},
		'MED':{'values':[1,2,3,2,1,2,3],'color':'green'},
		'WAS':{'values':range(10),'color':'red'},
		'SAS':{'values':range(10),'color':'red'},

		'SEA':{'values':range(10),'color':'red'},
		'NAU':{'values':range(10),'color':'red'},
		'SAU':{'values':range(10),'color':'red'},
		'global':{'values':range(10),'color':'red'}}


def small_plot(subax,region,info_dict):
	print('________'+region+'________')
	subax.plot(info_dict[region]['values'], color=info_dict[region]['color'])
	subax.annotate(region, xy=(0.93, 0.78), xycoords='axes fraction', color='k', weight='bold', fontsize=10, ha='right', backgroundcolor='w')

def axis_settings(subax,info_dict,label=False,region=None):
	# subax.set_title(period + ' ' + style)
	subax.set_xlim(0,10)
	subax.set_ylim(0,10)
	if region == 'global':
		subax.set_ylabel('y')
		subax.set_xlabel('x')
	else:
		subax.set_yticklabels([])
		subax.set_xticklabels([])
	subax.grid(True,which="both",ls="--",c='gray',lw=0.35)
	return(subax)

plt.close('all')
fig,ax_map=regional_panels_on_map.regional_panels_on_map(small_plot, axis_settings, polygons=srex, reg_info=srex_reg_info, x_ext=[-180,180], y_ext=[-65,85], small_plot_size=0.08, info_dict = info_dict, title=None)

plt.tight_layout(); plt.savefig('example.pdf'); plt.close()









#
