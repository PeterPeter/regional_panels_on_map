import seaborn as sns

import matplotlib.pylab as plt
from matplotlib.path import Path
import matplotlib
from matplotlib.collections import PatchCollection

import numpy as np

import cartopy.crs as ccrs
import cartopy

from shapely import geometry

def regional_panels_on_map(small_plot_function, axis_settings,  polygons, reg_info, info_dict, x_ext=[-180,180], y_ext=[-64,-2], small_plot_size=0.05, output_name=None, fontsize=8, title=None, projection=None, size=13):

	if projection is None:
		projection=ccrs.Robinson(central_longitude=0, globe=None)

	asp=float(x_ext[-1]-x_ext[0])/float(y_ext[-1]-y_ext[0])

	# settings for big plot image
	ratio=0.0
	plt.close('all')
	fig=plt.figure(figsize=(size,size/asp))
	ax_map=fig.add_axes([0,0,1,1],projection=projection, zorder=2)
	ax_map.set_global()
	ax_map.coastlines()
	ax_map.set_extent(x_ext+y_ext, crs=ccrs.PlateCarree())
	ax_map.axis('off')


	# ax_map.outline_patch.set_edgecolor('white')
	ratio=0.0

	patches,colors=[],[]
	for region in sorted(reg_info.keys()):
		# fill the outer subplot with whatever is defined in small_plot_function
		if 'pos' in reg_info[region].keys():
			x,y=reg_info[region]['pos']
		else:
			x,y=geometry.Polygon(polygons[region]['points']).centroid.xy
			x,y=x[0],y[0]
			if 'pos_off' in reg_info[region].keys():
				x+=reg_info[region]['pos_off'][0]
				y+=reg_info[region]['pos_off'][1]

		x=abs(x-x_ext[0])/float(abs(x_ext[1]-x_ext[0]))
		y=abs(y-y_ext[0])/float(abs(y_ext[1]-y_ext[0]))

		if 'scaling_factor' in reg_info[region].keys():
			zz = reg_info[region]['scaling_factor']
		else:
			zz = 1.

		ax = fig.add_axes([x-small_plot_size*zz/2.,y-(small_plot_size*zz*0.75*asp*0.5),small_plot_size*zz,small_plot_size*zz*0.75*asp], zorder=3)
		ax.fill_between([-99999,99999],[-9999999,-9999999],[999999,9999999],color='w', zorder=0)


		default_options={
			'edge':None,
			'color':'white',
			'hatch':'//',
			'linewidth':3,
			'alpha':0.3
		}
		for option in ['edge','color','hatch','linewidth','alpha']:
			if option not in reg_info[region].keys():
				reg_info[region][option]=default_options[option]

		if reg_info[region]['edge'] is not None:
			for pos in ['top', 'bottom', 'right', 'left']:
				ax.spines[pos].set_edgecolor(reg_info[region]['edge'])
				if reg_info[region]['edge'] == 'none':
					ax.spines[pos].set_edgecolor('gray')


		ax=axis_settings(ax,info_dict,label=True,region=region)

			# ax=axis_settings(ax,label=False,info_dict)

		small_plot_function(subax=ax,region=region,info_dict=info_dict)

		# add polygon
		if region in polygons.keys():
			# if reg_info[region]['edge'] is None:
			# 	reg_info[region]['edge'] = 'none'
			# ax_map.add_geometries([geometry.Polygon(polygons[region]['points'])], ccrs.PlateCarree(), color=reg_info[region]['edge'],alpha=reg_info[region]['alpha'],facecolor=reg_info[region]['color'],hatch=reg_info[region]['hatch'],linewidth=reg_info[region]['linewidth'])
			ax_map.add_geometries([geometry.Polygon(polygons[region]['points'])], ccrs.PlateCarree(), edgecolor=reg_info[region]['edge'],alpha=reg_info[region]['alpha'],facecolor=reg_info[region]['color'],linewidth=reg_info[region]['linewidth'])
			#ax_map.add_geometries([geometry.Polygon(polygons[region]['points'])], ccrs.PlateCarree())

	if title is not None:
		title_ax=fig.add_axes([0.2,0.8,0.6,0.2]);	title_ax.axis('off')
		title_ax.annotate(title, xy=(0.5,0.5), xycoords='axes fraction', fontsize=12,fontweight='bold',xytext=(-5, 5), textcoords='offset points',backgroundcolor='white',horizontalalignment='center')


	if output_name!=None: plt.savefig(output_name,dpi=600)
	if output_name==None: return(fig,ax_map)


def plot_polygon_legend(ax, polygons, reg_info, x_ext=[-180,180], y_ext=[-90,90], fontsize=5):
	ax.coastlines(color='gray',linewidth=0.5); ax.axis('off')
	ax.outline_patch.set_edgecolor('white')
	ax.set_extent(x_ext+y_ext, crs=ccrs.PlateCarree())
	for region in sorted(reg_info.keys()):
		if region in polygons.keys():
			poly = geometry.Polygon(polygons[region]['points'])
			ax.add_geometries([poly], ccrs.PlateCarree(), color=reg_info[region]['color'], alpha=reg_info[region]['alpha'], facecolor=reg_info[region]['color'])
			xy = tuple(poly.centroid.coords)[0]
			ax.annotate(region, xy, xycoords=ccrs.PlateCarree()._as_mpl_transform(ax), ha='center', fontsize=fontsize, backgroundcolor='w')
